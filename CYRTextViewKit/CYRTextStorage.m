//
//  CYRTextStorage.m
//
//  Version 0.4.0
//
//  Created by Illya Busigin on 01/05/2014.
//  Copyright (c) 2014 Cyrillian, Inc.
//
//  Distributed under MIT license.
//  Get the latest version from here:
//
//  https://github.com/illyabusigin/CYRTextView
//
// The MIT License (MIT)
//
// Copyright (c) 2014 Cyrillian, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#import "CYRTextStorage.h"
#import "CYRToken.h"

@interface CYRTextStorage ()
@property (nonatomic, strong) NSMutableAttributedString *storage;
@end

@implementation CYRTextStorage

#pragma mark - Initialization & Setup

- (instancetype)init
{
    if (self = [super init]) {
        self.storage = [[NSMutableAttributedString alloc] initWithString:@""];
        self.defaultFont = [UIFont systemFontOfSize:12.0f];
        self.defaultTextColor = [UIColor blackColor];
        _tokens = @[];
    }
    return self;
}

#pragma mark - Overrides

- (void)setTokens:(NSMutableArray *)tokens
{
    _tokens = tokens;
    [self highlightAll];
}

- (NSString *)string
{
    return self.storage.string;
}

- (NSDictionary *)attributesAtIndex:(NSUInteger)location effectiveRange:(NSRangePointer)range
{
    if (!self.storage.length) {
        return @{};
    }
    return [self.storage attributesAtIndex:location effectiveRange:range];
}

- (void)replaceCharactersInRange:(NSRange)range withString:(NSString *)str
{
    [self.storage replaceCharactersInRange:range withString:str];
    [self edited:NSTextStorageEditedCharacters range:range changeInLength:str.length - range.length];
}

- (void)setAttributes:(NSDictionary *)attrs range:(NSRange)range
{
    [self.storage setAttributes:attrs range:range];
    [self edited:NSTextStorageEditedAttributes range:range changeInLength:0];
}

- (void)processEditing
{
    [super processEditing];
    if (self.editedMask & NSTextStorageEditedCharacters) {
        NSRange range = [self.string paragraphRangeForRange:self.editedRange];
        [self highlight:range];
    }
}

- (void)highlightAll
{
    [self highlight:NSMakeRange(0, self.storage.length)];
}

- (void)highlight:(NSRange)range
{
    [self beginEditing];
    [self.storage addAttribute:NSForegroundColorAttributeName value:self.defaultTextColor range:range];
    for (CYRToken *token in self.tokens) {
        NSRegularExpression *regex = [token expression];
        [regex enumerateMatchesInString:self.string options:0 range:range usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
            if (token.index > 0 && result.numberOfRanges > token.index){
                [self.storage addAttributes:token.attributes range:[result rangeAtIndex:token.index]];
            }
            else {
                [self.storage addAttributes:token.attributes range:result.range];
            }
        }];
    }
    [self endEditing];
    [self edited:NSTextStorageEditedAttributes range:range changeInLength:0];
}

@end
